### Todo List App

#### Используемые технологии:

- JavaScript
- React
- React Hooks
- Less
- HTML5/CSS3

#### Запуск проекта:

```
git clone https://gitlab.com/test_group490/todo_list.git
cd todo_list
npm install
npm start
```
